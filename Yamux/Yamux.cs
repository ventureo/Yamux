/*
 *  Автор: KirMozor <https://gitlab.com/KirMozor>
 *  Пара функций для YamuxWindow
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using Gdk;
using GLib;
using Gtk;
using Newtonsoft.Json.Linq;
using Pango;
using Process = System.Diagnostics.Process;

namespace Yamux
{
    public class Yamux
    {
        public static List<Button> ListButtonPlay = new List<Button>();
        public static bool LockPlay = false;

        public static void OpenLinkToWebBrowser(string url)
        {
            Logger.logger.Debug("Вызвался метод Yamux.OpenLinkToWebBrowser()");
            try
            {
                Process.Start(url);
                Logger.logger.Debug("Запустился процесс url: " + url);
            }
            catch
            {
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                {
                    url = url.Replace("&", "^&");
                    Logger.logger.Debug("cmd /c start " + url);
                    Process.Start(new ProcessStartInfo("cmd", $"/c start {url}") { CreateNoWindow = true });
                }

                if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                {
                    Logger.logger.Debug("xdg-open " + url);
                    Process.Start("xdg-open", url);
                }

                if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
                {
                    Logger.logger.Debug("open " + url);
                    Process.Start("open", url);
                }
            }
        }

        public static Dictionary<string, List<string>> GetPlaylists(JToken root)
        {
            Logger.logger.Debug("Вызвался метод Yamux.GetPlaylists");
            Logger.logger.Debug("Создаю list<string> для добавления туда данных");
            Dictionary<string, List<string>> playlist = new Dictionary<string, List<string>>();
            List<string> type = new List<string>();
            List<string> playlistUid = new List<string>();
            List<string> playlistKind = new List<string>();
            List<string> playlistName = new List<string>();
            List<string> playlistCoverUri = new List<string>();

            Logger.logger.Debug("Начинаю перебор результата парсинга");
            type.Add("playlist");
            foreach (JToken i in root["playlists"]["results"])
            {
                Logger.logger.Debug("playlistUid.Add(" + i["uid"] + ") ; playlistKind.Add(" + i["kind"] +
                                    ") ; playlistName.Add(" + i["title"] + ")");
                playlistUid.Add(i["uid"].ToString());
                playlistKind.Add(i["kind"].ToString());
                playlistName.Add(i["title"].ToString());
                try
                {
                    Logger.logger.Debug("Получил url картинки: " + i["cover"]["uri"]);
                    playlistCoverUri.Add(i["cover"]["uri"].ToString());
                }
                catch (NullReferenceException)
                {
                    Logger.logger.Debug("Нету картинки");
                    playlistCoverUri.Add("None");
                }
            }

            Logger.logger.Debug("Добавляю все эти листы в один словарь");
            playlist.Add("type", type);
            playlist.Add("uid", playlistUid);
            playlist.Add("kind", playlistKind);
            playlist.Add("name", playlistName);
            playlist.Add("coverUri", playlistCoverUri);

            return playlist;
        }

        public static Dictionary<string, List<string>> GetPodcasts(JToken root)
        {
            Logger.logger.Debug("Вызвался метод Yamux.GetPodcasts");
            Logger.logger.Debug("Создаю list<string> для добавления туда данных");
            Dictionary<string, List<string>> podcast = new Dictionary<string, List<string>>();
            List<string> type = new List<string>();
            List<string> podcastId = new List<string>();
            List<string> podcastName = new List<string>();
            List<string> podcastCoverUri = new List<string>();

            Logger.logger.Debug("Начинаю перебор результата парсинга");
            type.Add("podcast");
            foreach (JToken i in root["podcast_episodes"]["results"])
            {
                Logger.logger.Debug("podcastId.Add(" + i["id"] + ") ; podcastName.Add(" + i["title"] + ")");
                podcastId.Add(i["id"].ToString());
                podcastName.Add(i["title"].ToString());
                try
                {
                    Logger.logger.Debug("Получил url картинки: " + i["coverUri"]);
                    podcastCoverUri.Add(i["coverUri"].ToString());
                }
                catch (NullReferenceException)
                {
                    Logger.logger.Debug("Нету картинки");
                    podcastCoverUri.Add("None");
                }
            }

            Logger.logger.Debug("Добавляю все эти листы в один словарь");
            podcast.Add("type", type);
            podcast.Add("id", podcastId);
            podcast.Add("name", podcastName);
            podcast.Add("coverUri", podcastCoverUri);

            return podcast;
        }

        public static Dictionary<string, List<string>> GetTracks(JToken root)
        {
            Logger.logger.Debug("Вызвался метод Yamux.GetTracks");
            Logger.logger.Debug("Создаю list<string> для добавления туда данных");
            Dictionary<string, List<string>> tracks = new Dictionary<string, List<string>>();
            List<string> type = new List<string>();
            List<string> trackId = new List<string>();
            List<string> trackName = new List<string>();
            List<string> trackCoverUri = new List<string>();

            Logger.logger.Debug("Начинаю перебор результата парсинга");
            type.Add("track");
            foreach (JToken i in root["tracks"]["results"])
            {
                Logger.logger.Debug("trackId.Add(" + i["id"] + ") ; trackName.Add(" + i["title"] + ")");
                trackId.Add(i["id"].ToString());
                trackName.Add(i["title"].ToString());
                try
                {
                    Logger.logger.Debug("Получил url картинки: " + i["coverUri"]);
                    trackCoverUri.Add(i["coverUri"].ToString());
                }
                catch (NullReferenceException)
                {
                    Logger.logger.Debug("Нету картинки");
                    trackCoverUri.Add("None");
                }
            }

            Logger.logger.Debug("Добавляю все эти листы в один словарь");
            tracks.Add("type", type);
            tracks.Add("id", trackId);
            tracks.Add("name", trackName);
            tracks.Add("coverUri", trackCoverUri);

            return tracks;
        }

        public static Dictionary<string, List<string>> GetArtist(JToken root)
        {
            Logger.logger.Debug("Вызвался метод Yamux.GetArtist");
            Logger.logger.Debug("Создаю list<string> для добавления туда данных");
            Dictionary<string, List<string>> artist = new Dictionary<string, List<string>>();
            List<string> type = new List<string>();
            List<string> artistId = new List<string>();
            List<string> artistName = new List<string>();
            List<string> artistCoverUri = new List<string>();

            Logger.logger.Debug("Начинаю перебор результата парсинга");
            type.Add("artist");
            foreach (JToken i in root["artists"]["results"])
            {
                Logger.logger.Debug("artistId.Add(" + i["id"] + ") ; artistName.Add(" + i["name"] + ")");
                artistId.Add(i["id"].ToString());
                artistName.Add(i["name"].ToString());

                try
                {
                    Logger.logger.Debug("Получил url картинки: " + i["cover"]["uri"]);
                    artistCoverUri.Add(i["cover"]["uri"].ToString());
                }
                catch (NullReferenceException)
                {
                    Logger.logger.Debug("Нету картинки");
                    artistCoverUri.Add("None");
                }
            }

            Logger.logger.Debug("Добавляю все эти листы в один словарь");
            artist.Add("type", type);
            artist.Add("id", artistId);
            artist.Add("name", artistName);
            artist.Add("coverUri", artistCoverUri);

            return artist;
        }

        public static Dictionary<string, List<string>> GetAlbums(JToken root)
        {
            Logger.logger.Debug("Вызвался метод Yamux.GetAlbums");
            Logger.logger.Debug("Создаю list<string> для добавления туда данных");
            Dictionary<string, List<string>> albums = new Dictionary<string, List<string>>();
            List<string> type = new List<string>();
            List<string> albumsId = new List<string>();
            List<string> albumsName = new List<string>();
            List<string> albumsCoverUri = new List<string>();

            Logger.logger.Debug("Начинаю перебор результата парсинга");
            type.Add("album");
            foreach (JToken i in root["albums"]["results"])
            {
                Logger.logger.Debug("albumsId.Add(" + i["id"] + ") ; albumsName.Add(" + i["title"] + ")");
                albumsId.Add(i["id"].ToString());
                albumsName.Add(i["title"].ToString());

                try
                {
                    Logger.logger.Debug("Получил url картинки: " + i["coverUri"]);
                    albumsCoverUri.Add(i["coverUri"].ToString());
                }
                catch (NullReferenceException)
                {
                    Logger.logger.Debug("Нету картинки");
                    albumsCoverUri.Add("None");
                }
            }

            Logger.logger.Debug("Добавляю все эти листы в один словарь");
            albums.Add("type", type);
            albums.Add("id", albumsId);
            albums.Add("name", albumsName);
            albums.Add("coverUri", albumsCoverUri);

            return albums;
        }

        public static Dictionary<string, string> GetBest(JToken root)
        {
            Logger.logger.Debug("Вызвался метод Yamux.GetBest");
            Dictionary<string, string> best = new Dictionary<string, string>();

            Logger.logger.Debug("Лучший результат: " + root["best"]["type"]);
            best.Add("type", root["best"]["type"].ToString());

            return best;
        }

        public static HBox CreateBoxResultSearch(List<string> name, List<string> coverUri, List<string> id,
            string typeResult)
        {
            Logger.logger.Debug("Вызвался метод Yamux.CreateBoxResultSearch");
            Logger.logger.Debug("Создался newBox, newBox.Valign = Align.Start");
            HBox newBox = new HBox();
            newBox.Valign = Align.Start;

            Logger.logger.Debug("Начинаю создание box");
            int b = -1;
            foreach (string i in name)
            {
                Logger.logger.Debug("Индекс (b): " + b);
                b++;
                Logger.logger.Debug("Создаём VBox coverImage для изображений и Button для воспроизведения");
                VBox coverImage = new VBox();
                Button buttonPlay = new Button();
                Logger.logger.Debug("");
                coverImage.Spacing = 4;
                coverImage.MarginTop = 20;
                coverImage.MarginBottom = 15;
                coverImage.Valign = Align.Start;
                coverImage.Halign = Align.Start;

                newBox.Add(coverImage);
                newBox.Spacing = 8;

                Label nameBestLabel = new Label(i);
                FontDescription tpfNameBest = new FontDescription();
                tpfNameBest.Size = 11264;
                nameBestLabel.ModifyFont(tpfNameBest);
                nameBestLabel.MaxWidthChars = 20;
                nameBestLabel.Ellipsize = EllipsizeMode.End;
                nameBestLabel.Halign = Align.Center;
                nameBestLabel.Valign = Align.Start;

                string uri;
                Pixbuf imagePixbuf = null;
                if (coverUri[b] != "None")
                {
                    uri = "https://" + coverUri[b].Replace("%%", "50x50");
                    HttpWebRequest request =
                        (HttpWebRequest)WebRequest.Create("https://" + coverUri[b].Replace("%%", "100x100"));
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    using (Stream stream = response.GetResponseStream())
                    {
                        imagePixbuf = new Pixbuf(stream);
                    }
                    response.Close();
                }
                else
                {
                    uri = null;
                }

                if (typeResult != "playlist")
                {
                    buttonPlay.Name = "{'type': \"" + typeResult + "\",'id': \"" + id[b] + "\", 'uri': \"" + uri +
                                      "\" }";
                }
                else
                {
                    buttonPlay.Name = "{ 'type': \"" + typeResult + "\", 'uid': \"" + Search.uidPlaylist[b] +
                                      "\", 'kind': \"" + Search.kindPlaylist[b] + "\", 'uri': \"" + uri + "\"}";
                }
                
                Image image = new Image();
                if (uri == null)
                {
                    image.IconName = "error";
                    image.UseFallback = true;
                    image.PixelSize = 100;
                }
                else
                {
                    image = new Image(imagePixbuf);
                }
                image.Halign = Align.Fill;
                buttonPlay.Image = image;
                buttonPlay.Relief = ReliefStyle.None;

                ListButtonPlay.Add(buttonPlay);
                coverImage.Add(buttonPlay);
                coverImage.Add(nameBestLabel);
                newBox.Add(coverImage);
            }

            return newBox;
        }
    }
}