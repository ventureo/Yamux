/*
 *  Автор: KirMozor <https://gitlab.com/KirMozor>
 *  Класс для работы с GStreamer, то есть плеером Yamux
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using Gst;
using Newtonsoft.Json.Linq;
using YandexMusicApi;
using UI = Gtk.Builder.ObjectAttribute;


namespace Yamux
{
    public class Player
    {
        public delegate void PlayerDelegate();

        private static bool boolStopTrack = false;
        private static bool StopAwait = false;

        private static bool PlayTrackOrNo;
        public static bool PlayTrackOrPause = true;
        public static Element pipeline;
        public static int currentTrack = -1;
        public static List<string> trackIds = new List<string>();

        public static event PlayerDelegate TrackLast;
        public static event PlayerDelegate TrackNext;
        public static event PlayerDelegate ChangeCurrentTrack;

        public static async void PlayUrlFile()
        {
            Logger.logger.Debug("Вызвали метод Player.PlayUrlFile");
            Logger.logger.Debug("Количество элементов в trackIds: " + trackIds.Count);
            Logger.logger.Debug("Инициализируем GStreamer");
            Application.Init();
            StopPlayIfPlay();
            Logger.logger.Debug("Присваиваем directLinkToTrack пустую строку");
            string directLinkToTrack = "";
            await System.Threading.Tasks.Task.Run(() =>
            {
                Logger.logger.Debug("Получаем прямую ссылку на трек с id: " + trackIds[0]);
                directLinkToTrack = GetDirectLinkWithTrack(trackIds[0]);
            });
            Logger.logger.Debug("Получил прямую ссылку: " + directLinkToTrack +
                                ". Присваиваем Search.directLink прямую ссылку");
            Search.directLink = directLinkToTrack;

            Logger.logger.Debug("Присваиваем currentTrack = -1");
            currentTrack = -1;
            Logger.logger.Debug("Инициализируем pipeline");
            pipeline = new Pipeline();
            Logger.logger.Debug("Присваем playbin pipeline_description: playbin uri=" + directLinkToTrack);
            pipeline = Parse.Launch("playbin uri=" + directLinkToTrack);

            Logger.logger.Debug("Вызываем event ChangeCurrentTrack");
            ChangeCurrentTrack.Invoke();
            Logger.logger.Debug("Включаем трек");
            pipeline.SetState(State.Playing);
            await System.Threading.Tasks.Task.Run(() => { SpyEndTrack(); });
        }

        public static void NextTrack(object sender, EventArgs a)
        {
            Logger.logger.Debug("Вызвали метод Player.NextTrack. Количество элементов в trackIds: " + trackIds.Count);
            //if (trackIds.Count > 1)
            //{
                TrackNext.Invoke();
            //}
        }

        public static void LastTrack(object sender, EventArgs a)
        {
            Logger.logger.Debug("Вызвали метод Player.LastTrack. Количество элементов в trackIds: " + trackIds.Count);
            if (trackIds.Count > 1)
            {
                TrackLast.Invoke();
            }
        }

        public static async void PlayPlaylist()
        {
            Logger.logger.Debug("Вызвали метод Player.PlayPlaylist(). Количество элементов в trackIds: " +
                                trackIds.Count);
            Logger.logger.Debug("Присваиваем currentTrack = -1");
            currentTrack = -1;
            Logger.logger.Debug("Запускается цикл");
            while (true)
            {
                try
                {
                    Logger.logger.Debug("Инициализируем GStreamer");
                    Application.Init();
                    StopPlayIfPlay();
                    currentTrack++;
                    Logger.logger.Debug("Прибавили к currentTrack + 1, до этого был равен: " + currentTrack);
                    StopAwait = false;
                    Logger.logger.Debug("Присваеваем StopAwait = false");

                    Logger.logger.Debug("Присваиваем directLinkToTrack пустую строку");
                    string directLinkToTrack = "";
                    await System.Threading.Tasks.Task.Run(() =>
                    {
                        Logger.logger.Debug("Получаем прямую ссылку на трек с id: " + trackIds[currentTrack]);
                        directLinkToTrack = GetDirectLinkWithTrack(trackIds[currentTrack]);
                    });
                    Logger.logger.Debug("Получил прямую ссылку: " + directLinkToTrack +
                                        ". Присваиваем Search.directLink прямую ссылку");
                    Search.directLink = directLinkToTrack;

                    Logger.logger.Debug("Инициализируем pipeline");
                    pipeline = new Pipeline();
                    Logger.logger.Debug("Присваем playbin pipeline_description: playbin uri=" + directLinkToTrack);
                    pipeline = Parse.Launch("playbin uri=" + directLinkToTrack);
                    Logger.logger.Debug("Включаем трек");
                    pipeline.SetState(State.Playing);
                    ChangeCurrentTrack.Invoke();
                    await System.Threading.Tasks.Task.Run(() => { SpyEndTrack(); });
                }
                catch (ArgumentOutOfRangeException)
                {
                    Logger.logger.Debug("Плейлист закончился. Переводим pipeline в State.Null");
                    pipeline.SetState(State.Null);
                    Logger.logger.Debug("Присваеваем StopAwait = true");
                    StopAwait = true;
                    Logger.logger.Debug("Присваеваем currentTrack = -1");
                    currentTrack = -1;
                    break;
                }
            }
        }

        public static void PauseOrStartPlay()
        {
            Logger.logger.Debug("Вызвали метод Player.PauseOrStartPlay(). PlayTrackOrPause = " + PlayTrackOrPause);
            if (PlayTrackOrPause)
            {
                Logger.logger.Debug("Ставим паузу pipeline.SetState(State.Paused)");
                pipeline.SetState(State.Paused);
                Logger.logger.Debug("Присваеваем PlayTrackOrNo и PlayTrackOrPause в false");
                //TODO: Посмотреть внимательнее на PlayTrackOrNo и PlayTrackOrPause, возникает ощущение что они бесполезные
                PlayTrackOrNo = false;
                PlayTrackOrPause = false;
            }
            else
            {
                Logger.logger.Debug("Включаем трек pipeline.SetState(State.Playing)");
                pipeline.SetState(State.Playing);
                Logger.logger.Debug("Присваеваем PlayTrackOrNo и PlayTrackOrPause в true");
                PlayTrackOrNo = true;
                PlayTrackOrPause = true;
            }
        }

        private static string GetDirectLinkWithTrack(string trackId)
        {
            Logger.logger.Debug("Вызвали метод Player.GetDirectLinkWithTrack, передали trackId: " + trackId);
            JObject result = Track.GetDownloadInfoWithToken(trackId);
            Logger.logger.Debug("Результат реквеста: " + result);

            //TODO: Переписать эту функцию, тут шиздец
            var resultData = result.GetValue("result");
            resultData = resultData[0];

            string url = resultData.ToList()[3].ToList()[0].ToString();
            Logger.logger.Debug("Распарсил ссылку: " + url + "\nВызывается метод Track.GetDirectLink");
            return Track.GetDirectLink(url);
        }

        public static async void DownloadUriWithThrottling(string url, string path, double speedKbps = -0.0)
        {
            Logger.logger.Debug("Вызывается метод Player.DownloadUriWithTrottling");
            System.Uri uri = new System.Uri(url);
            var req = WebRequest.CreateHttp(uri);
            using (var resp = await req.GetResponseAsync())
            using (var stream = resp.GetResponseStream())
            using (var outfile = File.OpenWrite(path))
            {
                var startTime = System.DateTime.Now;
                long totalDownloaded = 0;
                var buffer = new byte[0x10000];
                while (true)
                {
                    var actuallyRead = await stream.ReadAsync(buffer, 0, buffer.Length);
                    if (actuallyRead == 0)
                        return;
                    await outfile.WriteAsync(buffer, 0, actuallyRead);
                    totalDownloaded += actuallyRead;

                    if (speedKbps != -0.0)
                    {
                        var expectedTime = totalDownloaded / 1024.0 / speedKbps;
                        var actualTime = (System.DateTime.Now - startTime).TotalSeconds;
                        if (expectedTime > actualTime)
                            await System.Threading.Tasks.Task.Delay(TimeSpan.FromSeconds(expectedTime - actualTime));
                    }
                }
            }
        }

        public static void DownloadWithSync(System.Uri uri, string path, double speedKbps = -0.0)
        {
            Logger.logger.Debug("Вызывается метод Player.DownloadWithSync");
            var req = WebRequest.CreateHttp(uri);
            using (var resp = req.GetResponse())
            using (var stream = resp.GetResponseStream())
            using (var outfile = File.OpenWrite(path))
            {
                var buffer = new byte[0x10000];
                while (true)
                {
                    var actuallyRead = stream.Read(buffer, 0, buffer.Length);
                    if (actuallyRead == 0)
                        return;
                    outfile.Write(buffer, 0, actuallyRead);
                }
            }
        }

        private static void StopPlayIfPlay()
        {
            Logger.logger.Debug("Вызвали метод Player.StopPlayIfPlay");
            try
            {
                Logger.logger.Debug("Проверяем pipeline.CurrentState на соответствие условию: " +
                                    pipeline.CurrentState);
                if (pipeline.CurrentState == State.Playing ||
                    pipeline.CurrentState == State.Paused || pipeline.CurrentState == State.Ready)
                {
                    pipeline.SetState(State.Null);
                    StopAwait = true;
                }
            }
            catch (NullReferenceException)
            {
                Logger.logger.Error("Ошибка NullReferenceException");
            }
        }

        private static void SpyEndTrack()
        {
            Logger.logger.Debug("Вызвали метод Player.SpyEndTrack");
            Yamux.LockPlay = false;
            Logger.logger.Debug("Лочим Yamux.LockPlay, = false");
            TrackNext += () =>
            {
                Logger.logger.Debug("Тыкнули на кнопку TrackNext, присваеваем StopAwait = true");
                StopAwait = true;
            };
            TrackLast += () =>
            {
                Logger.logger.Debug("Тыкнули на кнопку TrackLast, currentTrack = " + currentTrack +
                                    " присваеваем StopAwait = true и отнимаем от currentTrack - 2");
                currentTrack -= 2;
                StopAwait = true;
            };

            while (true)
            {
                long duration;
                pipeline.QueryDuration(Format.Time, out duration);
                duration = duration / Constants.SECOND;
                Thread.Sleep(500);
                try
                {
                    long position;
                    pipeline.QueryPosition(Format.Time, out position);
                    position = position / Constants.SECOND;
                    if (pipeline.CurrentState == State.Playing && position == duration && position > 1 &&
                        duration > 1 || StopAwait)
                    {
                        Logger.logger.Debug(
                            "Трек закончился по условию, StopAwait = false, начинается следующий трек (наверно)");
                        StopAwait = false;
                        break;
                    }
                }
                catch (NullReferenceException ex)
                {
                    Logger.logger.Error(ex, "Ошибка NullReferenceException");
                }
            }
        }
    }
}