/*
 *  Автор: KirMozor <https://gitlab.com/KirMozor>
 *  Класс для работы с библиотекой пользователя
 */


using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Gtk;
using Newtonsoft.Json.Linq;
using YandexMusicApi;
using UI = Gtk.Builder.ObjectAttribute;


namespace Yamux;

public class MyLibraryWindow : Window
{
    [UI] private Label IfNoResult = null;
    [UI] private Image PlayerImage = null;
    [UI] private Label PlayerNameArtist = null;
    [UI] private Label PlayerTitleTrack = null;
    [UI] private Box ResultBox = null;
    [UI] private Spinner spinnerProgress = null;

    public MyLibraryWindow() : this(YamuxWindow.YamuxWindowBuilder)
    {
    }

    private MyLibraryWindow(Builder builder) : base(builder.GetRawOwnedObject("MainWindow"))
    {
        builder.Autoconnect(this);
        Logger.logger.Debug("Конектим билдер в MyLibraryWindow");
        Logger.logger.Debug("Вызываем функцию генерации UI");
        GenerateUI();
    }

    private async void GenerateUI()
    {
        spinnerProgress.Active = true;

        YamuxWindow.CleanMainBox();
        Search.uidPlaylist = new List<string>();
        Search.kindPlaylist = new List<string>();
        ResultBox.Add(YamuxWindow.MainBox);
        IfNoResult.Text = "";
        Search search = new Search();
        search.HidePlayer();

        JToken InformMe = "";
        await Task.Run(() => { InformMe = Account.ShowInformAccount()["result"]; });
        string uid = InformMe["account"]["uid"].ToString();

        JToken MyPlaylists = "";
        await Task.Run(() => { MyPlaylists = Playlist.GetPlaylistUser(uid)["result"]; });
        
        List<string> name = new List<string>();
        List<string> coverUri = new List<string>();
        if (MyPlaylists.Count() > 0)
        {
            foreach (var i in MyPlaylists)
            {
                name.Add(i["title"].ToString());
                Search.uidPlaylist.Add(i["uid"].ToString());
                Search.kindPlaylist.Add(i["kind"].ToString());
                coverUri.Add(i["ogImage"].ToString());
            }
        }

        try
        {
            JToken LikeTracks = "";
            await Task.Run(() => { LikeTracks = Playlist.InformPlaylist(uid, "3")["result"]; });
            coverUri.Add(LikeTracks["cover"]["itemsUri"][0].ToString().Replace("%%", "100x100"));
            name.Add("Мне нравится");
            Search.uidPlaylist.Add(LikeTracks["owner"]["uid"].ToString());
            Search.kindPlaylist.Add("3");

            foreach (var i in Search.kindPlaylist)
            {
                Console.WriteLine(i);
            }
        }
        catch (NullReferenceException ex)
        {
        }

        Box UserPlaylist = Yamux.CreateBoxResultSearch(name, coverUri, new List<string>(), "playlist");
        foreach (Button i in Yamux.ListButtonPlay)
        {
            i.Clicked += (sender, args) => { search.PlayButtonClick(i.Name); };
        }

        YamuxWindow.MainBox.Add(UserPlaylist);
        YamuxWindow.MainBox.ShowAll();
        spinnerProgress.Active = false;
    }
}