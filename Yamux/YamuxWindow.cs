/*
 *  Автор: KirMozor <https://gitlab.com/KirMozor>
 *  Тут основной код окна Yamux
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using Gdk;
using Gst;
using Gtk;
using Newtonsoft.Json.Linq;
using Tomlyn;
using YandexMusicApi;
using Application = Gtk.Application;
using Task = System.Threading.Tasks.Task;
using UI = Gtk.Builder.ObjectAttribute;
using Window = Gtk.Window;
using Process = System.Diagnostics.Process;
using Thread = System.Threading.Thread;


namespace Yamux
{
    class YamuxWindow : Window
    {
        public delegate void LenghtTrack();

        private static int durationTrack = 1;

        private static HScale PlayerScale = new HScale(0.0, 100, 1.0);

        private static Button myWaveButton = new Button();
        private static bool showAdditBoxOrNo = true;
        public static VBox MainBox = new VBox();
        public static Search search = null;
        public static string path = null;
        public static Builder YamuxWindowBuilder = new Builder("Yamux.glade");
        [UI] private Button AboutDonateMe = null;
        [UI] private Button AboutGitHubAuthor = null;

        [UI] private Button AboutGitHubProject = null;
        [UI] private Image AboutImage = null;
        [UI] private Button AboutTelegramChannel = null;
        [UI] private Window AboutWindow = null;
        [UI] private Box AdditBox = null;
        [UI] private Button CloseAboutWindow = null;
        [UI] private Button CloseDonateWindow = null;
        [UI] private Dialog DonateWindow = null;

        [UI] private Window HowToYourTheme = null;
        [UI] private Label IfNoResult = null;
        [UI] private Dialog InDeveloperment = null;
        [UI] private Button KofiDonate = null;
        [UI] private Button LandingPageButton = null;
        [UI] private Button LightButton = null;
        [UI] private ApplicationWindow LoginYamux = null;
        [UI] private Button LoginYamuxButton = null;
        [UI] private Button MyLibraryPageButton = null;
        [UI] private Button Ok = null;

        // Player
        [UI] private Box PlayerActionBox = null;
        [UI] private Button PlayerAddPlaylistTrack = null;
        [UI] private Box PlayerBoxScale = null;
        [UI] private Button PlayerDifferentTrack = null;
        [UI] private Button PlayerDownloadTrack = null;
        [UI] private Image PlayerImage = null;
        [UI] private Button PlayerLastTrack = null;
        [UI] private Button PlayerLikeTrack = null;
        [UI] private Label PlayerNameArtist = null;
        [UI] private Button PlayerNextTrack = null;
        [UI] private Button PlayerNotRecommendedTrack = null;
        [UI] private Button PlayerPauseTrack = null;
        [UI] private Image pauseTrackImage = null;
        [UI] private Button PlayerPlaylistShow = null;
        [UI] private Button PlayerRepeatTrack = null;
        [UI] private VolumeButton PlayerSelectVolume = null;
        [UI] private Label PlayerTitleTrack = null;
        [UI] private Button PodcastPageButton = null;
        [UI] private Window PreStartWindow = null;
        [UI] private Button ResetPassword = null;
        [UI] private Box ResultBox = null;
        [UI] private Label ResultLogin = null;

        private VBox ResultSearchBox = new VBox();

        [UI] private Button RotorPageButton = null;
        [UI] private Box SearchBox = null;
        [UI] private Entry SetLogin = null;
        [UI] private Entry SetPassword = null;
        [UI] private Button SettingsButton = null;
        [UI] private Button ShowAdditBoxButton = null;
        [UI] private Image ShowAdditBoxImage = null;
        [UI] private Spinner spinnerProgress = null;

        [UI] private Image YandexImage = null;

        public YamuxWindow() : this(YamuxWindowBuilder)
        {
            Logger.logger.Debug("");
            PlayerPauseTrack.Clicked += (sender, args) =>
            {
                if (Player.PlayTrackOrPause)
                {
                    pauseTrackImage.Destroy();
                    pauseTrackImage = new Image();
                    pauseTrackImage.IconName = "media-playback-start";
                    pauseTrackImage.PixelSize = 20;
                    pauseTrackImage.UseFallback = true;
                    PlayerPauseTrack.Image = pauseTrackImage;
                }
                else
                {
                    pauseTrackImage.Destroy();
                    pauseTrackImage = new Image();
                    pauseTrackImage.IconName = "media-playback-pause";
                    pauseTrackImage.PixelSize = 20;
                    pauseTrackImage.UseFallback = true;
                    PlayerPauseTrack.Image = pauseTrackImage;
                }

                Player.PauseOrStartPlay();
            };

            PlayerLastTrack.Clicked += Player.LastTrack;
            PlayerNextTrack.Clicked += Player.NextTrack;
            PlayerLikeTrack.Clicked += (sender, args) => { LikeCurrentTrack(); };
            PlayerDownloadTrack.Clicked += PlayerDownloadTrackOnClicked;
        }

        private YamuxWindow(Builder builder) : base(builder.GetRawOwnedObject("MainWindow"))
        {
            builder.Autoconnect(this);

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            }
            else
            {
                path = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            }

            if (!Directory.Exists(path + "/.local/share/Yamux"))
            {
                Directory.CreateDirectory(path + "/.local/share/Yamux");
            }

            DeleteEvent += (o, args) => { Application.Quit(); };
            if (Directory.Exists(path + "/.local/share/Yamux/Svg"))
            {
                try
                {
                    SetDefaultIconFromFile(path + "/.local/share/Yamux/Svg/yandex_en_icon-icons.com_61632(1).png");
                }
                catch (GLib.GException)
                {
                    Directory.Delete(path + "/.local/share/Yamux/Svg", true);
                    Process.Start(Environment.ProcessPath);
                    Environment.Exit(0);
                }
            }

            PreStart();
        }

        private static event LenghtTrack ChangeLengthTrack;

        private async void PreStart()
        {
            if (!Directory.Exists(path + "/.local/share/Yamux/Svg"))
            {
                Directory.CreateDirectory(path + "/.local/share/Yamux/Svg");
                File.Copy("about_icon.svg", path + "/.local/share/Yamux/Svg/about_icon.svg");
                File.Copy("icon.svg", path + "/.local/share/Yamux/Svg/icon.svg");
                File.Copy("yandex_en_icon-icons.com_61632(1).png", path + "/.local/share/Yamux/Svg/yandex_en_icon-icons.com_61632(1).png");
            }
            if (!File.Exists(path + "/.local/share/Yamux/config.toml"))
            {
                GetTokenGui();
            }
            else
            {
                PreStartWindow.ShowAll();
                using (FileStream fstream = File.OpenRead(path + "/.local/share/Yamux/config.toml"))
                {
                    byte[] buffer = new byte[fstream.Length];
                    fstream.Read(buffer, 0, buffer.Length);
                    string textFromFile = Encoding.Default.GetString(buffer);

                    var model = Toml.ToModel(textFromFile);
                    Token.token = (string)model["yandex_token"]!;
                }

                try
                {
                    await Task.Run(() => { Account.ShowSettings(); });
                    PreStartWindow.Hide();
                    LoginYamux.Hide();

                    ShowAdditBoxButton.Clicked += ShowAdditBox;
                    AboutDonateMe.Clicked += ShowDonateWindow;
                    Player.ChangeCurrentTrack += () => { ShowCurrentTrack(); };
                    SettingsButton.Clicked += ShowAboutWindow;
                    LandingPageButton.Clicked += (sender, args) => { GenerateLanding(); };
                    RotorPageButton.Clicked += (sender, args) => { GenerateOther("rotor"); };
                    Ok.Clicked += (sender, args) => { InDeveloperment.Hide(); };
                    PodcastPageButton.Clicked += (sender, args) => { InDeveloperment.Show(); };
                    MyLibraryPageButton.Clicked += (sender, args) => { GenerateOther("myLibrary"); };

                    SetDefaultIconFromFile(path + "/.local/share/Yamux/Svg/icon.svg");
                    AboutImage.Pixbuf = new Pixbuf(path + "/.local/share/Yamux/Svg/about_icon.svg");

                    GenerateLanding();
                    Search search = new Search();
                    search.HidePlayer();
                }
                catch (WebException)
                {
                    PreStartWindow.Hide();
                    LoginYamux.Hide();
                    GetTokenGui();
                }
            }
        }

        private void GenerateOther(string typeGenerate)
        {
            spinnerProgress.Active = true;
            Search search = new Search();
            search.HidePlayer();

            PlayerBoxScale.Hide();
            PlayerActionBox.Hide();
            switch (typeGenerate)
            {
                case "rotor":
                    new RotorWindow();
                    break;
                case "myLibrary":
                    new MyLibraryWindow();
                    break;
            }
        }

        private async void GenerateLanding()
        {
            IfNoResult.Text = "";
            CleanMainBox();
            ResultBox.Add(MainBox);

            myWaveButton = new Button();
            JToken myWaveImage = "";
            await Task.Run(() =>
            {
                myWaveImage = Rotor.GetInfo("user:onyourwave")["result"][0]["station"]["fullImageUrl"];
            });

            myWaveImage = myWaveImage.ToString().Replace("%%", "100x100");
            await Task.Run(() =>
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://" + myWaveImage);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                using (System.IO.Stream stream = response.GetResponseStream())
                {
                    Pixbuf imagePixbuf = new Pixbuf(stream);
                    Image waveButtonImage = new Image();
                    waveButtonImage.Pixbuf = imagePixbuf;
                    myWaveButton.Image = waveButtonImage;
                }

                response.Close();
            });

            myWaveButton.Relief = ReliefStyle.None;
            myWaveButton.Clicked += PlayMyWave;

            List<string> name = new List<string>();
            List<string> coverUri = new List<string>();

            JToken generatedPlaylistsReq = "";
            await Task.Run(() =>
            {
                generatedPlaylistsReq = YandexMusicApi.Default.GetAllFeed()["result"]["generatedPlaylists"];
            });

            foreach (var i in generatedPlaylistsReq)
            {
                name.Add(i["data"]["title"].ToString());
                coverUri.Add(i["data"]["cover"]["uri"].ToString());
                Search.uidPlaylist.Add(i["data"]["uid"].ToString());
                Search.kindPlaylist.Add(i["data"]["kind"].ToString());
            }

            Box generatedPlaylistsBox = Yamux.CreateBoxResultSearch(name, coverUri, new List<string>(), "playlist");

            Search search = new Search();
            foreach (Button i in Yamux.ListButtonPlay)
                i.Clicked += (sender, args) => { search.PlayButtonClick(i.Name); };

            MainBox.Add(myWaveButton);
            MainBox.Add(generatedPlaylistsBox);
            MainBox.ShowAll();
            SearchBox.ShowAll();
            search = new Search();
            search.HidePlayer();
        }

        private async void PlayMyWave(object sender, EventArgs a)
        {
            bool StopAwait = false;
            bool StopWave = false;
            while (true)
            {
                Player.trackIds = new List<string>();
                JObject myWaveReturn = new JObject();
                await Task.Run(() => { myWaveReturn = Rotor.GetTrack("user:onyourwave"); });
                Console.WriteLine(Rotor.GetInfo("user:onyourwave"));
                string ids = myWaveReturn["result"]["sequence"][0]["track"]["id"].ToString();

                ChangeLengthTrack += () => { PlayerScale.Value = durationTrack; };
                Player.TrackNext += () => { StopAwait = true; };
                Player.trackIds.Add(ids);
                Player.PlayUrlFile();

                await Task.Run(() =>
                {
                    while (true)
                    {
                        Thread.Sleep(1000); //Просто сон async метода, чтобы пк не офигел от проверки
                        try
                        {
                            long duration;
                            Player.pipeline.QueryDuration(Format.Time, out duration);
                            duration /= Constants.SECOND;
                            long position;
                            Player.pipeline.QueryPosition(Format.Time, out position);
                            position /= Constants.SECOND;

                            if (duration == position && duration != 0 && duration != -1 ||
                                StopAwait) //Если трек закончился, начать следующию песню
                            {
                                StopAwait = false;
                                Player.pipeline.SetState(Gst.State.Ready);
                                break;
                            }
                        }
                        catch (NullReferenceException)
                        {
                        }
                    }
                });
            }
        }

        public static void CleanMainBox()
        {
            MainBox.Destroy();
            MainBox = new VBox();
        }

        private void ShowAdditBox(object sender, EventArgs a)
        {
            if (showAdditBoxOrNo)
            {
                AdditBox.Show();
                showAdditBoxOrNo = false;
            }
            else
            {
                AdditBox.Hide();
                showAdditBoxOrNo = true;
            }
        }

        private void ShowAboutWindow(object sender, EventArgs a)
        {
            AboutWindow.ShowAll();
            AboutWindow.Deletable = false;

            CloseAboutWindow.Clicked += (o, args) => { AboutWindow.Hide(); };
            AboutGitHubProject.Clicked += (o, args) =>
            {
                Yamux.OpenLinkToWebBrowser("https://gitlab.com/KirMozor/Yamux");
            };
            AboutGitHubAuthor.Clicked += (o, args) => { Yamux.OpenLinkToWebBrowser("https://gitlab.com/KirMozor"); };
            AboutTelegramChannel.Clicked += (o, args) => { Yamux.OpenLinkToWebBrowser("https://t.me/kirmozor"); };
        }

        private void ShowDonateWindow(object sender, EventArgs a)
        {
            DonateWindow.ShowAll();
            DonateWindow.Deletable = false;
            CloseDonateWindow.Clicked += (o, args) => { DonateWindow.Hide(); };
            KofiDonate.Clicked += (o, args) => { Yamux.OpenLinkToWebBrowser("https://ko-fi.com/kirmozor"); };
        }

        private async void LikeCurrentTrack()
        {
            string myUid = "";
            await Task.Run(() =>
            {
                myUid = Account.ShowInformAccount()["result"]["account"]["uid"].ToString();
                if (Player.currentTrack != -1)
                {
                    Track.LikesTracks(new List<string>() { Player.trackIds[Player.currentTrack] }, myUid);
                }
                else
                {
                    Track.LikesTracks(new List<string>() { Player.trackIds[0] }, myUid);
                }
            });
        }

        private async void ShowCurrentTrack()
        {
            try
            {
                pauseTrackImage = new Image();
                pauseTrackImage.IconName = "media-playback-pause";
                pauseTrackImage.UseFallback = true;
                pauseTrackImage.PixelSize = 10;
                
                if (Player.currentTrack != -1 || Player.trackIds.Count == 1)
                {
                    JToken trackInform = "";
                    if (Player.currentTrack != -1)
                    {
                        await Task.Run(() =>
                        {
                            trackInform = Track.GetInformTrack(new List<string>()
                                { Player.trackIds[Player.currentTrack] })["result"][0];
                        });
                    }
                    else
                    {
                        await Task.Run(() =>
                        {
                            trackInform =
                                Track.GetInformTrack(new List<string>() { Player.trackIds[0] })["result"][0];
                        });
                    }

                    Console.WriteLine(trackInform);

                    PlayerImage.IconName = "error";
                    PlayerImage.UseFallback = true;
                    PlayerImage.PixelSize = 100;
                    
                    PlayerTitleTrack.Text = trackInform["title"].ToString();
                    PlayerNameArtist.Text = trackInform["artists"][0]["name"].ToString();
                    PlayerScale.Show();
                    PlayerBoxScale.ShowAll();
                    PlayerActionBox.ShowAll();
                    await Task.Run(() =>
                    {
                        string url = "https://" +
                                     trackInform["albums"][0]["coverUri"].ToString().Replace("%%", "50x50");
                        Console.WriteLine(url);
                        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                        using (System.IO.Stream stream = response.GetResponseStream())
                        {
                            PlayerImage.Pixbuf = new Pixbuf(stream);
                        }

                        response.Close();
                    });
                    PlayerImage.ShowAll();
                }
            }
            catch (ArgumentOutOfRangeException)
            {
            }
        }

        private void GetTokenGui()
        {
            if (Directory.Exists(path + "/.local/share/Yamux/Svg"))
            {
                YandexImage.Pixbuf =
                    new Pixbuf((path + "/.local/share/Yamux/Svg/yandex_en_icon-icons.com_61632(1).png"));
            }

            if (File.Exists(path + "/.local/share/Yamux/config.toml"))
            {
                File.Delete(path + "/.local/share/Yamux/config.toml");
            }

            LoginYamux.ShowAll();
            ResetPassword.Clicked += (sender, args) => { Login.ResetPasswordOpen(); };
            LoginYamuxButton.Clicked += (sender, args) =>
            {
                ResultLogin.Text = Login.LogInButton(SetLogin.Text, SetPassword.Text);
                if (ResultLogin.Text == "ok")
                {
                    PreStart();
                }
            };
        }

        private void PlayerDownloadTrackOnClicked(object sender, EventArgs a)
        {
            if (Directory.Exists(path + "/YandexMusic") == false)
            {
                Directory.CreateDirectory(path + "/YandexMusic/");
            }

            string nameTrackFile =
                path + "/YandexMusic/" + PlayerNameArtist.Text + " - " + PlayerTitleTrack.Text + ".mp3";
            Console.WriteLine(Search.directLink);
            Player.DownloadUriWithThrottling(Search.directLink, nameTrackFile);
        }
    }

    static class Ext
    {
        public static void Move<T>(this List<T> list, int i, int j)
        {
            var elem = list[i];
            list.RemoveAt(i);
            list.Insert(j, elem);
        }

        public static void Swap<T>(this List<T> list, int i, int j)
        {
            var elem1 = list[i];
            var elem2 = list[j];

            list[i] = elem2;
            list[j] = elem1;
        }
    }
}