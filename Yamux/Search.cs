/*
 *  Автор: KirMozor <https://gitlab.com/KirMozor>
 *  Тут класс для работы с поиском, выдачи результатов и передачи сигнала о воспроизведении в Player
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Gdk;
using Gtk;
using Newtonsoft.Json.Linq;
using Pango;
using YandexMusicApi;
using UI = Gtk.Builder.ObjectAttribute;
using Window = Gtk.Window;


namespace Yamux
{
    class Search : Window
    {
        private delegate void LenghtTrack();

        private static int durationTrack = 1;
        public static string directLink;

        public static List<string> uidPlaylist = new List<string>();
        public static List<string> kindPlaylist = new List<string>();
        private static bool SearchOrNot = true;
        private static HScale PlayerScale = new HScale(0.0, 100, 1.0);
        [UI] private Label IfNoResult = null;

        [UI] private Box PlayerActionBox = null;
        [UI] private Button PlayerAddPlaylistTrack = null;
        [UI] private Box PlayerBoxScale = null;
        [UI] private Button PlayerDifferentTrack = null;
        [UI] private Button PlayerDownloadTrack = null;
        [UI] private Image PlayerImage = null;
        [UI] private Button PlayerLastTrack = null;
        [UI] private Label PlayerNameArtist = null;
        [UI] private Button PlayerNextTrack = null;
        [UI] private Button PlayerNotRecommendedTrack = null;
        [UI] private Button PlayerPauseTrack = null;
        [UI] private Image pauseTrackImage = null;
        [UI] private Button PlayerPlaylistShow = null;
        [UI] private Button PlayerRepeatTrack = null;
        [UI] private Button PlayerSelectQaulity = null;
        [UI] private Button PlayerSelectVolume = null;
        [UI] private Label PlayerTitleTrack = null;
        [UI] private Box ResultBox = null;
        [UI] private Box SearchBox = null;
        [UI] private SearchEntry SearchMusic = null;
        [UI] private Spinner spinnerProgress = null;

        public Search() : this(YamuxWindow.YamuxWindowBuilder)
        {
        }

        private Search(Builder builder) : base(builder.GetRawOwnedObject("MainWindow"))
        {
            builder.Autoconnect(this);
            Logger.logger.Debug("Конектим билдер в Search");
            SearchMusic.SearchChanged += (object sender, EventArgs a) => { SearchChangedOutput(); };
        }
        
        private async void SearchChangedOutput()
        {
            string text = SearchMusic.Text;
            JToken root = "";
            await Task.Run(() =>
            {
                Thread.Sleep(2000);
                if (text == SearchMusic.Text && !string.IsNullOrEmpty(SearchMusic.Text) && !string.IsNullOrEmpty(text))
                {
                    //TODO: Переписать
                    spinnerProgress.Active = true;
                    JObject resultSearch = YandexMusicApi.Default.Search(text);
                    root = resultSearch.Last.Last.Root;
                    root = root.SelectToken("result");
                    spinnerProgress.Active = false;
                }
            });
            ShowResultSearch(root, text);
        }

        private async void ShowResultSearch(JToken root, string text)
        {
            Logger.logger.Debug("Вызвали метод Search.ShowResultSearch");
            Logger.logger.Debug("Текст поискового запроса " + text);
            Logger.logger.Debug("Текст пустой? " + !string.IsNullOrEmpty(text) + ";" +
                                !string.IsNullOrEmpty(SearchMusic.Text));
            //TODO: Глянуть получше на SearchMusic.Text
            if (text == SearchMusic.Text && !string.IsNullOrEmpty(SearchMusic.Text) && !string.IsNullOrEmpty(text))
            {
                try
                {
                    Logger.logger.Debug("Проверяем выдачу на то, что результат есть");
                    JToken asd = root["best"];
                    Logger.logger.Debug("Результат есть, SearchOrNot = " + SearchOrNot);
                    if (SearchOrNot)
                    {
                        Logger.logger.Debug("Начинается парсинг результата поиска. Лочим параллельный поиск");
                        SearchOrNot = false;
                        spinnerProgress.Active = true;
                        IfNoResult.Text = "";
                        HidePlayer();
                        YamuxWindow.CleanMainBox();
                        Logger.logger.Debug("Добавляем в ResultBox YamuxWindow.MainBox");
                        ResultBox.Add(YamuxWindow.MainBox);

                        Dictionary<string, string> best = Yamux.GetBest(root);
                        List<Dictionary<string, List<string>>> all = new List<Dictionary<string, List<string>>>();

                        try
                        {
                            all.Add(Yamux.GetArtist(root));
                        }
                        catch (NullReferenceException)
                        {
                        }

                        try
                        {
                            all.Add(Yamux.GetAlbums(root));
                        }
                        catch (NullReferenceException)
                        {
                        }

                        try
                        {
                            all.Add(Yamux.GetTracks(root));
                        }
                        catch (NullReferenceException)
                        {
                        }

                        try
                        {
                            all.Add(Yamux.GetPodcasts(root));
                        }
                        catch (NullReferenceException)
                        {
                        }

                        try
                        {
                            all.Add(Yamux.GetPlaylists(root));
                        }
                        catch (NullReferenceException)
                        {
                        }

                        Logger.logger.Debug("Запускаем цикл по переборке в поисках лучшего результат");
                        int index = -1;
                        foreach (var i in all)
                        {
                            index++;
                            Logger.logger.Debug("Тип в JSON ответе: " + i["type"][0]);
                            Logger.logger.Debug("Тип в best: " + best["type"]);
                            Logger.logger.Debug("Индекс: " + index);
                            if (i["type"][0] == best["type"])
                            {
                                Logger.logger.Debug("Перемещаем лучший результат на 1 место");
                                all.Move(index, 0);
                                break;
                            }
                        }

                        Logger.logger.Debug("Запускаем цикл по перебору all");
                        foreach (var i in all)
                        {
                            Logger.logger.Debug(
                                "Создаём HBox box ; ScrolledWindow scrolledWindow ; Viewport viewportWindow");
                            HBox box = new HBox();
                            ScrolledWindow scrolledWindow = new ScrolledWindow();
                            Viewport viewportWindow = new Viewport();
                            Logger.logger.Debug("Устанавливаем PropagateNaturalHeight в scrolledWindow в true");
                            scrolledWindow.PropagateNaturalHeight = true;

                            await Task.Run(() =>
                            {
                                Logger.logger.Debug("Создаём box с типом " + i["type"][0]);
                                if (i["type"][0] != "playlist")
                                    box = Yamux.CreateBoxResultSearch(i["name"], i["coverUri"], i["id"], i["type"][0]);
                                else
                                {
                                    kindPlaylist = i["kind"];
                                    uidPlaylist = i["uid"];
                                    box = Yamux.CreateBoxResultSearch(i["name"], i["coverUri"], new List<string>(),
                                        i["type"][0]);
                                }

                                Logger.logger.Debug("Устанавливаем box.Halign = Align.Start");
                                box.Halign = Align.Start;
                            });
                            string typeResult = "";
                            switch (i["type"][0])
                            {
                                case "playlist":
                                {
                                    typeResult = "Плейлисты";
                                    break;
                                }
                                case "album":
                                {
                                    typeResult = "Альбомы";
                                    break;
                                }
                                case "podcast":
                                {
                                    typeResult = "Выпуски подкастов";
                                    break;
                                }
                                case "track":
                                {
                                    typeResult = "Треки";
                                    break;
                                }
                                case "artist":
                                {
                                    typeResult = "Артисты";
                                    break;
                                }
                            }

                            Logger.logger.Debug("Тип текущего элемента: " + typeResult);

                            Logger.logger.Debug("Создаём label с размером щрифта 12");
                            Label trackLabel = new Label(typeResult);
                            FontDescription tpftrack = new FontDescription();
                            tpftrack.Size = 12288;
                            trackLabel.ModifyFont(tpftrack);

                            Logger.logger.Debug("Добавляем в scrolledWindow viewportWindow");
                            scrolledWindow.Add(viewportWindow);
                            Logger.logger.Debug("Добавляем в viewportWindow box");
                            viewportWindow.Add(box);
                            Logger.logger.Debug("Добавляем в YamuxWindow.MainBox trackLabel");
                            YamuxWindow.MainBox.Add(trackLabel);
                            Logger.logger.Debug("Добавляем в YamuxWindow.MainBox scrolledWindow");
                            YamuxWindow.MainBox.Add(scrolledWindow);
                            Logger.logger.Debug("Показываем всю выдачу");
                            ResultBox.ShowAll();
                        }

                        Logger.logger.Debug("Подключаем кнопки к PlayButtonClick");
                        foreach (Button i in Yamux.ListButtonPlay)
                            i.Clicked += (sender, args) => { PlayButtonClick(i.Name); };
                        Logger.logger.Debug("Разлочиваем поиск");
                        SearchOrNot = true;
                        spinnerProgress.Active = false;
                    }
                }
                catch (NullReferenceException)
                {
                    Logger.logger.Debug("Нет результата, root.Count = " + root.Count());
                    if (SearchOrNot)
                    {
                        Logger.logger.Debug("Поиск не идёт");
                    }
                    else
                    {
                        Logger.logger.Debug("Поиск уже идёт");
                    }

                    HidePlayer();
                    YamuxWindow.CleanMainBox();
                    Logger.logger.Debug("Устанавливаем в ifNoResult текст: Нет результата😢");
                    IfNoResult.Text = "Нет результата😢";
                }
            }
        }

        public void HelloFucker()
        {
            Console.WriteLine(("Hello fuck!"));
        }

        public async void PlayButtonClick(string dataButton)
        {
            Logger.logger.Debug("Вызвали метод Search.PlayButtonClick");
            Logger.logger.Debug("Проверяем залочен ли запуск трека: " + Yamux.LockPlay);
            if (Yamux.LockPlay == false)
            {
                Logger.logger.Debug("Лочим запуск трека: Yamux.LockPlay = true");
                Yamux.LockPlay = true;
                //TODO: Проверить Player.PlayTrackOrPause на надобность
                Player.PlayTrackOrPause = true;
                
                Logger.logger.Debug("Устанавливаем в плеере иконку паузы");
                pauseTrackImage = new Image();
                pauseTrackImage.IconName = "media-playback-pause";
                pauseTrackImage.UseFallback = true;
                pauseTrackImage.PixelSize = 20;
                    
                Logger.logger.Debug("Инициализируем Player.trackIds и устанавливаем Player.currentTrack = -1");
                Player.trackIds = new List<string>();
                Player.currentTrack = -1;
                Logger.logger.Debug("Получаем имя кнопки");
                JObject details = JObject.Parse(dataButton);
                Logger.logger.Debug(
                    "Устанавливаем максимальную длину символов в PlayerTitleTrack и PlayerNameArtist = 17");
                PlayerTitleTrack.MaxWidthChars = 17;
                PlayerNameArtist.MaxWidthChars = 17;

                Logger.logger.Debug("Тип воспроизводимого элемента: " + details["type"]);
                Logger.logger.Debug("ID этого элемента: " + details["id"]);
                //TODO: Глянь, тут вроде можно оптимизировать код
                switch (details["type"].ToString())
                {
                    case "track":
                    {
                        Logger.logger.Debug("Инициализируем лист ids");
                        List<string> ids = new List<string>();
                        JToken informTrack = "{}";

                        Logger.logger.Debug("Добавляем в ids id трека");
                        ids.Add(details["id"].ToString());

                        await Task.Run(() => { informTrack = Track.GetInformTrack(ids)["result"]; });
                        Logger.logger.Debug("Получил информацию о треке: \n" + informTrack);

                        Logger.logger.Debug("Титул трека: " + informTrack[0]["title"]);
                        Logger.logger.Debug("Имя артиста: " + informTrack[0]["artists"][0]["name"]);
                        PlayerTitleTrack.Text = informTrack[0]["title"].ToString();
                        PlayerNameArtist.Text = informTrack[0]["artists"][0]["name"].ToString();
                        Logger.logger.Debug("Добавляем в Player.trackIds трек id: " + details["id"]);
                        Player.trackIds.Add(details["id"].ToString());
                        Player.PlayUrlFile();
                        break;
                    }
                    case "artist":
                    {
                        JToken informArtist = "{}";
                        JToken trackArtist = "{}";

                        await Task.Run(
                            () => { informArtist = Artist.InformArtist(details["id"].ToString())["result"]; });
                        Logger.logger.Debug("Получил информацию о артисте");
                        await Task.Run(
                            () => { trackArtist = Artist.GetTrack(informArtist["artist"]["id"].ToString()); });
                        Logger.logger.Debug("Получил треки артиста\nНачинаю добавлять их в Player.trackIds");
                        foreach (var i in trackArtist["result"]["tracks"])
                        {
                            Logger.logger.Debug(i["id"]);
                            Player.trackIds.Add(i["id"].ToString());
                        }

                        Player.PlayPlaylist();
                        break;
                    }
                    case "playlist":
                    {
                        JToken informPlaylist = "{}";
                        JToken trackPlaylist = "{}";

                        await Task.Run(() =>
                        {
                            informPlaylist =
                                Playlist.InformPlaylist(details["uid"].ToString(), details["kind"].ToString());
                        });
                        Logger.logger.Debug("Получил информацию о плейлисте");
                        await Task.Run(() =>
                        {
                            trackPlaylist = Playlist.GetTrack(details["uid"].ToString(),
                                details["kind"].ToString());
                        });
                        Logger.logger.Debug("Получил треки в плейлисте\nНачинаю добавлять их в Player.trackIds");
                        foreach (var i in trackPlaylist["result"]["tracks"])
                        {
                            Logger.logger.Debug(i["id"]);
                            Player.trackIds.Add(i["id"].ToString());
                        }

                        Player.PlayPlaylist();
                        break;
                    }
                    case "album":
                    {
                        JToken informAlbum = "{}";
                        JToken trackAlbum = "{}";

                        await Task.Run(() => { informAlbum = Album.InformAlbum(details["id"].ToString()); });
                        Logger.logger.Debug("Получил информацию о альбоме");
                        await Task.Run(() => { trackAlbum = Album.GetTracks(details["id"].ToString()); });
                        Logger.logger.Debug("Получил треки в альбоме\nНачинаю добавлять их в Player.trackIds");
                        foreach (var i in trackAlbum["result"]["volumes"][0])
                        {
                            Logger.logger.Debug(i["id"]);
                            Player.trackIds.Add(i["id"].ToString());
                            Console.WriteLine(i["id"]);
                        }

                        Player.PlayPlaylist();

                        break;
                    }
                    case "podcast":
                    {
                        List<string> ids = new List<string>();
                        JToken informPodcast = "{}";

                        ids.Add(details["id"].ToString());
                        await Task.Run(() => { informPodcast = Track.GetInformTrack(ids)["result"]; });
                        Logger.logger.Debug("Получил информацию о подкасте");
                        Logger.logger.Debug("Добавляю в Player.trackIds id: " + details["id"]);
                        Player.trackIds.Add(details["id"].ToString());
                        Player.PlayPlaylist();
                        break;
                    }
                }
            }
        }

        public void HidePlayer()
        {
            Logger.logger.Debug("Вызвали метод Search.HidePlayer()");
            try
            {
                Logger.logger.Debug("Текущее состояние Player: " + Player.pipeline.CurrentState);
                Logger.logger.Debug("Проходит ли по условию или нет? " +
                                    (Player.pipeline.CurrentState != Gst.State.Playing &&
                                     Player.pipeline.CurrentState != Gst.State.Paused));
                if (Player.pipeline.CurrentState != Gst.State.Playing &&
                    Player.pipeline.CurrentState != Gst.State.Paused)
                {
                    Logger.logger.Debug("Скрываем плеер");
                    PlayerNameArtist.Text = "";
                    PlayerTitleTrack.Text = "";
                    PlayerImage.Hide();
                    PlayerBoxScale.Hide();
                    PlayerActionBox.Hide();
                }
            }
            catch (NullReferenceException ex)
            {
                Logger.logger.Error(ex, "Ошибка, трек не запущен");
                Logger.logger.Debug("Скрываем плеер");
                PlayerNameArtist.Text = "";
                PlayerTitleTrack.Text = "";
                PlayerImage.Hide();
                PlayerBoxScale.Hide();
                PlayerActionBox.Hide();
            }
        }
    }
}