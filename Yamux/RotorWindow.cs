/*
 *  Автор: KirMozor <https://gitlab.com/KirMozor>
 *  Тут код для радиостанций
 */

using System.Threading.Tasks;
using Gtk;
using Newtonsoft.Json.Linq;
using YandexMusicApi;
using UI = Gtk.Builder.ObjectAttribute;

namespace Yamux;

public class RotorWindow : Window
{
    [UI] private Label IfNoResult = null;
    [UI] private Image PlayerImage = null;
    [UI] private Label PlayerNameArtist = null;
    [UI] private Label PlayerTitleTrack = null;
    [UI] private Box ResultBox = null;
    [UI] private Spinner spinnerProgress = null;

    public RotorWindow() : this(YamuxWindow.YamuxWindowBuilder)
    {
    }

    private RotorWindow(Builder builder) : base(builder.GetRawOwnedObject("MainWindow"))
    {
        builder.Autoconnect(this);
        Logger.logger.Debug("Конектим билдер в MyLibraryWindow");
        Logger.logger.Debug("Вызываем функцию генерации UI");
        GenerateUI();
    }

    private async void GenerateUI()
    {
        spinnerProgress.Active = true;

        Logger.logger.Debug("Удаляем текст из IfNoResult");
        IfNoResult.Text = "";
        Logger.logger.Debug("Запускаем очистку предыдущего результата");
        YamuxWindow.CleanMainBox();
        Logger.logger.Debug("Инициализируем класс Search в объект search");
        Search search = new Search();
        Logger.logger.Debug("Вызываем функцию search.HidePlayer");
        search.HidePlayer();
        Logger.logger.Debug("Создаётся Grid для добавления туда списка радиостанций");
        Grid sdasd = new Grid();
        Logger.logger.Debug("Делаем расстояние между рядами и колонками 100");
        sdasd.RowSpacing = 100;
        sdasd.ColumnSpacing = 100;


        Logger.logger.Debug("Добавляем в Yamux.MainBox Grid");
        YamuxWindow.MainBox.Add(sdasd);
        Logger.logger.Debug("Добавляем в ResultBox Yamux.MainBox");
        ResultBox.Add(YamuxWindow.MainBox);

        JToken stationList = "";
        await Task.Run(() =>
        {
            Logger.logger.Debug("Получаем список радиостанций");
            stationList = Rotor.StationList()["result"];
        });

        Logger.logger.Debug("Устанавливаем labelPositionHeight = 1 и labelPositonWidth = false");
        int labelPositionHeight = 1;
        bool labelPositonWidth = false;
        Logger.logger.Debug("Запускаем цикл перебора списка радиостанций");
        foreach (var i in stationList)
        {
            Label k = new Label(i["station"]["name"].ToString());
            Logger.logger.Debug("Получил имя радиостанции: " + k.Text + ". Установил MarginStart для k = 9");
            k.MarginStart = 9;
            Logger.logger.Debug("labelPositonWidth = " + labelPositonWidth + "; labelPositionHeight = " +
                                labelPositionHeight);
            if (labelPositonWidth)
            {
                sdasd.Attach(k, 0, 0, 6, labelPositionHeight);
                labelPositonWidth = false;
                labelPositionHeight++;
            }
            else
            {
                sdasd.Attach(k, 0, 0, 1, labelPositionHeight);
                labelPositonWidth = true;
            }
        }

        Logger.logger.Debug("Цикл закончился, показываю содержимое YamuxWindow.MainBox");
        YamuxWindow.MainBox.ShowAll();
        spinnerProgress.Active = false;
    }
}