/*
 *  Автор: KirMozor <https://gitlab.com/KirMozor>
 *  Класс для авторизации в YandexMusic
 */

using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System;
using Newtonsoft.Json.Linq;
using YandexMusicApi;
using Tomlyn;

namespace Yamux
{
    public class Login
    {
        public static void WriteToken(string text)
        {
            Logger.logger.Debug("Вызвался метод Login.WriteToken");
            text = "yandex_token = '" + text + "'";
            Logger.logger.Debug("Файл по пути: " + YamuxWindow.path + "/.local/share/Yamux/config.toml");
            using (FileStream fstream =
                   new FileStream(Path.GetFullPath(YamuxWindow.path + "/.local/share/Yamux/config.toml"),
                       FileMode.OpenOrCreate))
            {
                Logger.logger.Debug("Конвертирую текст в байты");
                byte[] buffer = Encoding.Default.GetBytes(text);
                Logger.logger.Debug("Записываю");
                fstream.Write(buffer, 0, buffer.Length);
            }
        }

        public static void ResetPasswordOpen()
        {
            Logger.logger.Debug("Вызвался метод Login.ResetPasswordOpen");
            string url = "https://passport.yandex.kz/auth/restore/login?mode=add-user";
            try
            {
                Process.Start(url);
                Logger.logger.Debug("Запустил процесс Process.Start(" + url + ")");
            }
            catch
            {
                Logger.logger.Debug("Сработало исключение");
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                {
                    url = url.Replace("&", "^&");
                    Logger.logger.Debug("Url: " + url + "\n Запускаемый процесс: cmd, аргументы: /c start " + url);
                    Process.Start(new ProcessStartInfo("cmd", $"/c start {url}") { CreateNoWindow = true });
                }
                else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                {
                    Logger.logger.Debug("Url: " + url + "\n Запускаемый процесс: xdg-open, аргументы: " + url);
                    Process.Start("xdg-open", url);
                }
                else if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
                {
                    Logger.logger.Debug("Url: " + url + "\n Запускаемый процесс: open, аргументы: " + url);
                    Process.Start("open", url);
                }
                else
                {
                    Logger.logger.Debug("Платформа не определенна :(");
                    throw;
                }
            }
        }

        public static string LogInButton(string loginText, string passwordText)
        {
            Logger.logger.Debug("Вызвался метод Login.LogInButton");
            bool loginCheck = String.IsNullOrWhiteSpace(loginText);
            Logger.logger.Debug("loginCheck пустой? : " + loginCheck);
            bool passwordCheck = String.IsNullOrWhiteSpace(passwordText);
            Logger.logger.Debug("loginCheck пустой? : " + loginCheck);

            if (loginCheck == false && passwordCheck == false)
            {
                try
                {
                    Logger.logger.Debug("Получаем токен");
                    JObject token = Token.GetToken(loginText, passwordText);
                    Logger.logger.Debug("Получил токен");
                    JToken jTokentoken = token.First.First;
                    WriteToken(jTokentoken.ToString());
                }
                catch (System.Net.WebException ex)
                {
                    Logger.logger.Error(ex, "Ошибка, " + ex.Status);
                    if (ex.Message == "The remote server returned an error: (400) Bad Request.")
                    {
                        return "400: Данные не верные";
                    }

                    if (ex.Message == "The remote server returned an error: (403) Forbidden.")
                    {
                        return "403: Yandex включил защиту от брутфорса";
                    }
                }
            }
            else
            {
                Logger.logger.Debug("Пользователь ввёл пустую строку");
                return "Вы ввели пустую строку. Введите пароль и логин";
            }

            return "ok";
        }
    }
}