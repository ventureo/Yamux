/*
 *  Автор: KirMozor <https://gitlab.com/KirMozor>
 *  А отсюда стартует Yamux
 */

using System.Net;
using Gtk;
using System;
using System.Drawing;
//using Console = Colorful.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;

namespace Yamux
{
    class Program
    {
        public static void Main()
        {
            var os = Environment.OSVersion;
            Logger.logger.Debug("\nИнформация о OS:");
            Logger.logger.Debug("Платформа: {0:G}", os.Platform);
            if (OperatingSystem.IsFreeBSD())
            {
                Logger.logger.Debug("Это FreeBSD");
            }

            Logger.logger.Debug("Версия: {0}", os.VersionString);
            Logger.logger.Debug("Информация о версии:");
            Logger.logger.Debug("   Мажорная версия: {0}", os.Version.Major);
            Logger.logger.Debug("   Минорная версия: {0}", os.Version.Minor);
            Logger.logger.Debug("Service Pack: '{0}'", os.ServicePack);
            Logger.logger.Debug("Путь до исполняемого файла: " + Environment.ProcessPath);
            if (Environment.Is64BitProcess)
            {
                Logger.logger.Debug("Процесс запущен в 64 бит");
            }
            else
            {
                Logger.logger.Debug("Процесс запущен в 32 бит");
            }

            try
            {
                Application.Init();
                Logger.logger.Debug("\nИнициализация приложения");

                Application app = new Application(IntPtr.Zero);
                app.Register(GLib.Cancellable.Current);
                YamuxWindow win = new YamuxWindow();
                Logger.logger.Debug("Инициализация класса YamuxWindow");
                app.AddWindow(win);
                Logger.logger.Debug("Добавилось новое окно в app");

                win.Show();
                Logger.logger.Debug("Показ окна");
                Application.Run();
                Logger.logger.Debug("Запуск приложения");
                Logger.logger.Debug("Инициализация класса YamuxWindow");
                app.AddWindow(win);
                Logger.logger.Debug("Добавилось новое окно в app");

                win.Show();
                Logger.logger.Debug("Показ окна");
                Application.Run();
                Logger.logger.Debug("Запуск приложения");
            }
            catch (WebException ex)
            {
                Logger.GetLogger().Error(ex, "WebException");
                Console.WriteLine(ex.Message);
            }
        }
    }
}