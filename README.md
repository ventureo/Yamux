# Yamux
![Linux](https://img.shields.io/badge/-Linux-grey?logo=linux)
![Licence](https://img.shields.io/badge/License-GPL_3.0-44cc11.svg)

**Описание:** Это клиент Яндекс.Музыки для Unix-подобных систем написанный на C# с использованием самописного API ([YandexMusicApi](https://gitlab.com/KirMozor/YandexMusicApi "Репозиторий самого API (C#)"))

## Скриншоты
<a href="Yamux/Screenshots/1_Login.png" title="Окно входа в Я.Аккаунт">
<img src="Yamux/Screenshots/1_Login.png" 
width="109" height="116" border="0"></a>

<a href="Yamux/Screenshots/2_Main_without_menu.png" title="Главная страница (без меню)">
<img src="Yamux/Screenshots/2_Main_without_menu.png" 
width="274" height="150" border="0"></a>

<a href="Yamux/Screenshots/3_Search_with_menu_and_now_playing_track.png" title="Демонстрация поиск (с меню и с включённым треком Король и Шута)">
<img src="Yamux/Screenshots/3_Search_with_menu_and_now_playing_track.png" 
width="274" height="150" border="0"></a>

## Установка Yamux'а
Пока доступна только сборка программы с исходников, также доступна возможность установить пакет с [AUR](https://aur.archlinux.org/packages/yamux "AUR - yamux") (только Arch GNU/Linux).

#### Установка Yamux'а с AUR'а для чайников
```
git clone https://aur.archlinux.org/yamux.git
cd yamux
makepkg -si
```
Запуск происходит через меню приложений или через комманду `yamux` в терминале.

## Сборка Yamux'а
### Зависимости
* `gstreamer` (для звука)
* `dotnet-runtime-6.0`, `dotnet-sdk-6.0` (для, неожиданно, сборки самого Yamux'а)
* `gtk3` (для GUI)

### Установка зависимостей
**Arch GNU/Linux:**
```
sudo pacman -S gtk3 gstreamer dotnet-runtime-6.0 dotnet-sdk-6.0
```

**Debian GNU/Linux | Ubuntu Linux:**
```
wget https://packages.microsoft.com/config/debian/11/packages-microsoft-prod.deb -O packages-microsoft-prod.deb \
sudo apt install ./packages-microsoft-prod.deb \
rm packages-microsoft-prod.deb \
sudo apt update; \
sudo apt install -y dotnet-runtime-6.0 dotnet-sdk-6.0 libgtk-3-0 libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev libgstreamer-plugins-bad1.0-dev gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly gstreamer1.0-libav gstreamer1.0-x gstreamer1.0-alsa gstreamer1.0-pulseaudio
```

### Итак, я установил все нужные зависимости, что дальше? 
Дальше, введите следующие команды:

```
git clone https://gitlab.com/KirMozor/Yamux.git
cd Yamux
dotnet publish -r linux-x64 #или dotnet publish -r linux-x32 (смотря какая битность у вас)
```
А теперь файл для запуска будет лежать по пути `Yamux/bin/Debug/net6.0/linux-x64/publish/Yamux`. Наслаждайтесь!

### Кстатиии

У меня есть телеграм блог куда я пишу прогресс разработки. Ссылочка внизу, подписывайся!

https://t.me/kirmozor

### Используемые библиотеки:
- [GStreamer-Sharp for .NET Core](https://github.com/vladkol/gstreamer-netcore)
- [YandexMusicApi](https://gitlab.com/KirMozor/YandexMusicApi)
- [GtkSharp](https://github.com/GtkSharp/GtkSharp)
- [Tomlyn](https://github.com/xoofx/Tomlyn)
- [Newtonsoft.Json](https://github.com/JamesNK/Newtonsoft.Json)

### Благодарности:
- Благодарю [Аристарха Бахирева](https://gitlab.com/AristarhBahirev "Aristarh Bahirev - GitLab") за то, что сделал мне [иконки для выбора качества](https://gitlab.com/KirMozor/Yamux/-/tree/main/Yamux/Svg/dark/high-quality.svg)
- Благодарю [Lameni](http://lameni.neonarod.com "Lameni - Telegram") за то, что помог мне улучшить README.MD
